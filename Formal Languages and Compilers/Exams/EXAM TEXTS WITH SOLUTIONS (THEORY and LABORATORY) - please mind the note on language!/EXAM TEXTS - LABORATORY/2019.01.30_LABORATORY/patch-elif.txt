diff -Naur ./acse_1.1.3/acse/Acse.lex ./acse_1.1.3_elif/acse/Acse.lex
--- ./acse_1.1.3/acse/Acse.lex	2018-06-13 10:11:49.092117000 +0200
+++ ./acse_1.1.3_elif/acse/Acse.lex	2019-01-25 09:35:57.378517337 +0100
@@ -97,6 +97,7 @@
 "return"          { return RETURN; }
 "read"            { return READ; }
 "write"           { return WRITE; }
+"elif"				{ return ELIF; }
 
 {ID}              { yylval.svalue=strdup(yytext); return IDENTIFIER; }
 {DIGIT}+          { yylval.intval = atoi( yytext );
diff -Naur ./acse_1.1.3/acse/Acse.y ./acse_1.1.3_elif/acse/Acse.y
--- ./acse_1.1.3/acse/Acse.y	2018-06-13 10:11:49.092117000 +0200
+++ ./acse_1.1.3_elif/acse/Acse.y	2019-01-28 14:32:58.636918075 +0100
@@ -94,7 +94,7 @@
 extern int yyerror(const char* errmsg);
 
 %}
-%expect 1
+%expect 2
 
 /*=========================================================================
                           SEMANTIC RECORDS
@@ -125,6 +125,7 @@
 %token READ
 %token WRITE
 
+
 %token <label> DO
 %token <while_stmt> WHILE
 %token <label> IF
@@ -132,11 +133,14 @@
 %token <intval> TYPE
 %token <svalue> IDENTIFIER
 %token <intval> NUMBER
+%token <label> ELIF
 
 %type <expr> exp
 %type <decl> declaration
 %type <list> declaration_list
 %type <label> if_stmt
+%type <label> elif_list
+%type <label> elif_block
 
 /*=========================================================================
                           OPERATOR PRECEDENCES
@@ -331,8 +335,26 @@
                   /* fix the `label_else' */
                   assignLabel(program, $2);
                }
+					| if_stmt ELIF 
+					{
+					   $2 = newLabel(program);
+   
+                  /* exit from the if-else */
+                  gen_bt_instruction (program, $2, 0);
+   
+                  // fix the label on the elif (first) case
+                  assignLabel(program, $1);
+					}
+					elif_list ELSE 
+					code_block
+               {
+                  // fix the labels referring to end
+                  assignLabel(program, $2);
+                  // gen_nop_instruction(program);
+		            assignLabel(program, $4);
+               }
 ;
-            
+
 if_stmt  :  IF
                {
                   /* the label that points to the address where to jump if
@@ -353,6 +375,45 @@
                code_block { $$ = $1; }
 ;
 
+elif_list: elif_block { 
+								// generate label_end for the current elif_list
+								$$ = newLabel(program);
+								
+								// if elif_block was executed then jump out  
+								gen_bt_instruction(program, $$,0);
+								
+								// fix label on the next case
+								assignLabel(program,$1);
+							 }
+			| elif_list ELIF 
+			  elif_block { 	
+				
+				// if elif_block was executed then jump out
+				gen_bt_instruction (program, $1, 0);
+				
+				// fix label on the next case
+				assignLabel(program,$3);	
+				
+				$$ = $1;	
+			}
+;			
+			
+elif_block: LPAR exp RPAR {
+            	if ($2.expression_type == IMMEDIATE)
+  		             gen_load_immediate(program, $2.value);
+               else
+                   gen_andb_instruction(program, $2.value, $2.value, $2.value, CG_DIRECT_ALL);
+               
+               $<label>$ = newLabel(program);
+               
+               // jump to the next case
+               gen_beq_instruction (program, $<label>$, 0);
+				 }
+				 code_block{
+				 	$$ = $<label>4;
+				 }
+;
+
 while_statement  : WHILE
                   {
                      /* initialize the value of the non-terminal */
diff -Naur ./acse_1.1.3/tests/elif/elif.src ./acse_1.1.3_elif/tests/elif/elif.src
--- ./acse_1.1.3/tests/elif/elif.src	1970-01-01 01:00:00.000000000 +0100
+++ ./acse_1.1.3_elif/tests/elif/elif.src	2019-01-28 11:38:53.333559779 +0100
@@ -0,0 +1,21 @@
+int a,b,c;
+
+read(a);
+read(c);
+
+if (a==0) write(0);
+elif (a==1){
+	write(1);
+	read(b);
+	if (b==10) 
+		if (c==100) write(100);
+		elif (c==101) write(101);
+		elif (c==102) write(102);
+		else write(103);
+	elif (b==11) write(11);
+	else write(12);
+}
+elif (a==2) 
+	if (c==100) write(100);
+	else write(103);
+else write(3);
