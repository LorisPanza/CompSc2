/***********uncapacitated-lotsizing.mod************/


/*************Point 1************/
param n;		#number of month in the life span
set J:=1..n;	#set of months

param m;		#max number of unit that the company can produce in one month
param c;		#unit cost of production
param e;		#max number of unit that can be bought from the other company each month
param c1;		#unit cost of a unit bought from the other company
param i;		#inventory cost for 1 unit, per month
param d{J};		#demand for month j

var x{J} >= 0;				#number of units produced in month j
var y{J} >= 0;				#number of units bought from the other company in month j
var z{J union {0}} >= 0;	#number of units stored in month j

#Cost: production cost + buying cost + inventory cost
minimize totalCost:
	sum{j in J}( c*x[j] + c1*y[j] + i*z[j] );
	
subject to demand{j in J}:
	x[j] + y[j] + z[j - 1] >= d[j];

subject to maxProduction{j in J}:
	x[j] <= m;

subject to maxBuying{j in J}:
	y[j] <= e;

subject to initializationInventory:
	z[0] = 0;

subject to balancing{j in J}:
	z[j] = x[j] + y[j] + z[j-1] - d[j];

/**********Point 2***********/

param l;		#minimum lot size in case of activation of production line

var o{J} binary;			#o[j] = 1 if the production is opened in month j

subject to activation{j in J}:
	x[j] <= o[j]*m;
	
subject to minimumLotSize{j in J}:
	x[j] >= o[j]*l;



data;

param n := 3;
param m := 110;
param c := 300;
param e := 60;
param c1 := 330;
param i := 10;
param l := 15;
param d:= 
	1	100,
	2	130,
	3	150;