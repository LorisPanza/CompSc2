set VMS;
set R_VMS;

param t_0;
param t_n > t_0;
set T := t_0 .. t_n;

param C >= 0;
param QOS_MIN := 0.8;

param USAGE_MIN := 0.6;
param NUM_DOWN_MAX := 3;

param r_dem{i in VMS, t in T} >= 0;
param R{i in VMS} >= 0;
param c_dem{t in T} := sum{i in VMS} r_dem[i, t];

var C_sys >= 1;
var r_al{VMS, T} >= 0;
var c_al{T} >= 0;
var u{T} >= 0;

#subject to QoS_perc{i in VMS, t in T}:
#	(r_al[i, t] / r_dem[i, t]) >= QOS_MIN;

subject to QoS{i in VMS}:
	count {t in T} (r_dem[i, t] > r_al[i, t]) <= NUM_DOWN_MAX;

#subject to QoS:
#	min{i in VMS}
#		(sum{t in T}(r_al[i, t] / r_dem[i, t]))/card(T) >= QOS_MIN;
		
subject to C_demand {t in T}:
	c_al[t] <= C_sys;

subject to r_allocation{i in VMS, t in T}:
	r_al[i,t]<= r_dem[i, t];
	
subject to r_allocation_2{i in VMS, t in T}:
	r_al[i, t] >= if R[i] > 0 and r_dem[i, t] < R[i]
			then r_dem[i, t]
			else R[i];
			
subject to c_allocation{t in T}:
	c_al[t] = sum{i in VMS} r_al[i, t];

subject to usage_min{t in T}:
	(c_al[t] / C_sys) >= USAGE_MIN;
	
#subject to usage_avg:
#	sum{t in T}((c_al[t] / C_sys) / card(T)) >= USAGE_MIN;
	
#maximize u_min: min{t in T} u[t];

#minimize down_min:
#	max{i in VMS}(count {t in T} (r_dem[i, t] > r_al[i, t]));

#minimize i_lost_min: sum{t in T}(C*(1-u[t])*C_sys) + sum{t in T}
#											sum{i in VMS}
#												(C*(r_dem[i,t] - r_al[i,t]));


data;
set VMS := vm1 vm2 vm3;
param t_0 := 1;
param t_n := 25;

param C:= 10;
param R:=
		vm1	500
		vm2	500
		vm3	1000;
param r_dem(tr): 
		vm1	vm2	vm3	:=
	1	387	33	2366
	2	326	33	2384
	3	2551	33	2364
	4	193	170	2339
	5	2592	114	2308
	6	195	173	2265
	7	2564	158	2175
	8	2542	173	2190
	9	2572	173	2339
	10	2551	174	2185
	11	173	405	2053
	12	174	204	1761
	13	196	196	2236
	14	2513	200	2289
	15	314	193	2213
	16	204	195	2288
	17	301	196	2384
	18	207	202	2356
	19	387	387	2188
	20	2547	326	2294
	21	200	333	2297
	22	33	386	2266
	23	274	207	2292
	24	333	245	2321
	25	2515	274	2287
