# SETS

param n;
set I := 1..n;

param m;
set J := 1..m;

# PARAMS

param t{I,J};
param p{J};
param q{J};
param T_min;
param T_max;

