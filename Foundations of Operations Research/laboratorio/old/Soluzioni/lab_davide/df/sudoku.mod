# SETS

param n;
set I := 1 .. n*n;

# PARAMS

param A{I,I} default 0;

# VARS

var x{i in I,j in I,k in I} binary, >= if A[i,j] = k then 1 else 0;

