{-
 - exercise on monads
 - MPradella 2012
 -}

{- Text:
 - Define a simple variant of the state monad, where the state is an integer
 - number, and in which every application of a bind increments such number.
 -}


-- data definition
data Inc a = Inc (Int -> (Int, a))

-- monad definition
instance Monad Inc where
	return x = Inc (\s -> (s, x))
	Inc a >>= f  =  Inc (\cnt -> 
			 let (cnt', v) = a cnt
			     Inc v'    = f v
			 in  v' (cnt'+1))


-- let's try it
pass :: Inc Int
pass = return 0

					
esmm :: Inc Int
esmm = do x <- return 1
	  pass
	  pass
	  x <- return (x+1)
	  pass
	  pass
          return (x+1)


tryesmm = let Inc a = esmm
	  in  a 0
